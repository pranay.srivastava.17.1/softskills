 An HTML document has numerous tags in it changing it or modifying each of them is a bit tough and hectic. But, having a programming language that can be used for manipulating and making changes to the webpage will make things easy. As the tags in the HTML document cannot be accessed or understood by the programming language so we need an interface that can convert or provide the accessibility of the same webpage in two different formats.    
&nbsp;

-----------------------------------------------------
### What is DOM?

**Document Object Model** is a programming interface that defines the standard for accessing web documents(HTML files). In layman's terms an HTML ***document*** in which every tag is defined as ***object*** in a certain hierarchical ***model*** that can be accessed by the scripting language JavaScript for manipulating the body, structure, content, format, etc of the webpage.  
 &nbsp;

![DOM Access Model](https://gitlab.com/pranay.srivastava.17.1/softskills/-/raw/main/Screenshot_from_2021-08-22_15-57-37_1_.jpg)
-------------------------------------------------------

### How does it help?

 A webpage is also a document that can be viewed by the browser or the HTML text file. The problem arises when we cannot make diverse changes in the webpage, here DOM will provide access for object-oriented representation of that same webpage so that it can be modified with JavaScript by applying changes in a broader way to get more responsive and active web pages. The DOM is not part of the JavaScript language but is instead built using multiple APIs that work together to build websites.
&nbsp;

-------------------------------------------------------
### Access DOM
 For accessing DOM we don't need any special kind of library or plugin. It is used by API directly run by JavaScript within the inline script tag in HTML code or a separate script file. But we should separate the script file so that code looks clean and understandable.

We access the HTML tags and elements with JavaScript by the following functions:
1. elements by id
The following example returns `id = "main"`
```javascript
const x = document.getElementById("main");
```
2. elements by tag name
The following example returns all <p> elements in the HTML Document.
```javascript
const element = document.getElementsByTagName("p"); 
```
3. elements by class name

```javascript
const x = document.getElementsByClassName("intro");
```
4. elements by CSS selectors
```javascript
const x = document.querySelectorAll("p.intro"); 
```
5. elements by HTML object collections
This example finds the form element with `id="frm1"`, in the forms collection, and displays all element values:
```javascript
const x = document.forms["frm1"];
let text = "";
for (let i = 0; i < x.length; i++) {
  text += x.elements[i].value + "<br>";
}
document.getElementById("demo").innerHTML = text;
```
&nbsp;

------------------------------------------------------
### DOM JS helper Methods

The Document Object represents all the possible nodes of the HTML Document file. These nodes are structured in a tree format which is considered a DOM tree. Each node can be addressed and modified with functions that are called methods in JavaScript.

&nbsp;
* elementIndex
```javascript
    export function elementIndex(e) {
        var idx = 0;
        while((e = e.previousElementSibling)) {
            idx++;
        }
        return idx;
    }
```

* indexInParent
```javascript
    export function indexInParent(e) {
    let child = e.parentNode.childNodes;
    let num = 0;

    for (let i = 0; i < child.length; i++) {
        if (child[i] == e) return num;
        if (child[i].nodeType == 1) num++;
    }
    return -1;
}
```

* indexOfParent
```javascript
    export function indexOfParent(e) {
        return [].indexOf.call(e.parentElement.children, e);
    }
```

* matches
```javascript
export function matches(elem, selector) {
    const isMsMatch = 'msMatchesSelector' in elem && elem.msMatchesSelector(selector);
    const isMatchSelector = 'matchesSelector' in elem && elem.matchesSelector(selector)
    const isMatch = 'matches' in elem && elem.matches(selector);
    // Test the element to see if it matches the provided selector
    // use different methods for compatibility
    return isMsMatch || isMatchSelector || isMatch;
    // Return the result of the test
    // If any of the above variables is true, the return value will be true
}
```

* closest
For each element in the set, get the first element that matches the selector by testing the element itself and traversing up through its ancestors in the DOM tree.
Depends on `matches;`

* next
Get the immediately following sibling of each element in the set of matched elements.
Depends on `matches, prev;`

* prev 
Get the immediately preceding sibling of each element in the set of matched elements.
Depends on `matches;`

* siblings
Get the siblings of each element in the set of matched elements.
Depends on `matches;`

&nbsp;

There are many more methods which is used by JavaScript for getting better cover and modifications of the webpage you can view with code examples in the references down below. 

&nbsp;

------------------------------------------------------
### References
* [About DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)
* [W3Schools](https://www.w3schools.com/js/js_htmldom.asp)
* [DOM JS helper Methods](https://jimfrenette.com/javascript/document/)